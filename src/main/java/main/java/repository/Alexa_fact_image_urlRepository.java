package main.java.repository;

import main.java.domain.Alexa_fact_image_url;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Alexa_fact_image_url entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Alexa_fact_image_urlRepository extends JpaRepository<Alexa_fact_image_url, Long> {

}
