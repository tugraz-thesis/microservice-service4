package main.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import main.java.domain.Alexa_fact_image_url;
import main.java.repository.Alexa_fact_image_urlRepository;
import main.java.web.rest.errors.BadRequestAlertException;
import main.java.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Alexa_fact_image_url.
 */
@RestController
@RequestMapping("/api")
public class Alexa_fact_image_urlResource {

    private final Logger log = LoggerFactory.getLogger(Alexa_fact_image_urlResource.class);

    private static final String ENTITY_NAME = "alexa_fact_image_url";

    private final Alexa_fact_image_urlRepository alexa_fact_image_urlRepository;

    public Alexa_fact_image_urlResource(Alexa_fact_image_urlRepository alexa_fact_image_urlRepository) {
        this.alexa_fact_image_urlRepository = alexa_fact_image_urlRepository;
    }

    /**
     * POST  /alexa-fact-image-urls : Create a new alexa_fact_image_url.
     *
     * @param alexa_fact_image_url the alexa_fact_image_url to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alexa_fact_image_url, or with status 400 (Bad Request) if the alexa_fact_image_url has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/alexa-fact-image-urls")
    @Timed
    public ResponseEntity<Alexa_fact_image_url> createAlexa_fact_image_url(@RequestBody Alexa_fact_image_url alexa_fact_image_url) throws URISyntaxException {
        log.debug("REST request to save Alexa_fact_image_url : {}", alexa_fact_image_url);
        if (alexa_fact_image_url.getId() != null) {
            throw new BadRequestAlertException("A new alexa_fact_image_url cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Alexa_fact_image_url result = alexa_fact_image_urlRepository.save(alexa_fact_image_url);
        return ResponseEntity.created(new URI("/api/alexa-fact-image-urls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alexa-fact-image-urls : Updates an existing alexa_fact_image_url.
     *
     * @param alexa_fact_image_url the alexa_fact_image_url to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alexa_fact_image_url,
     * or with status 400 (Bad Request) if the alexa_fact_image_url is not valid,
     * or with status 500 (Internal Server Error) if the alexa_fact_image_url couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/alexa-fact-image-urls")
    @Timed
    public ResponseEntity<Alexa_fact_image_url> updateAlexa_fact_image_url(@RequestBody Alexa_fact_image_url alexa_fact_image_url) throws URISyntaxException {
        log.debug("REST request to update Alexa_fact_image_url : {}", alexa_fact_image_url);
        if (alexa_fact_image_url.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Alexa_fact_image_url result = alexa_fact_image_urlRepository.save(alexa_fact_image_url);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, alexa_fact_image_url.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alexa-fact-image-urls : get all the alexa_fact_image_urls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of alexa_fact_image_urls in body
     */
    @GetMapping("/alexa-fact-image-urls")
    @Timed
    public List<Alexa_fact_image_url> getAllAlexa_fact_image_urls() {
        log.debug("REST request to get all Alexa_fact_image_urls");
        return alexa_fact_image_urlRepository.findAll();
    }

    /**
     * GET  /alexa-fact-image-urls/:id : get the "id" alexa_fact_image_url.
     *
     * @param id the id of the alexa_fact_image_url to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alexa_fact_image_url, or with status 404 (Not Found)
     */
    @GetMapping("/alexa-fact-image-urls/{id}")
    @Timed
    public ResponseEntity<Alexa_fact_image_url> getAlexa_fact_image_url(@PathVariable Long id) {
        log.debug("REST request to get Alexa_fact_image_url : {}", id);
        Optional<Alexa_fact_image_url> alexa_fact_image_url = alexa_fact_image_urlRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(alexa_fact_image_url);
    }

    /**
     * DELETE  /alexa-fact-image-urls/:id : delete the "id" alexa_fact_image_url.
     *
     * @param id the id of the alexa_fact_image_url to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/alexa-fact-image-urls/{id}")
    @Timed
    public ResponseEntity<Void> deleteAlexa_fact_image_url(@PathVariable Long id) {
        log.debug("REST request to delete Alexa_fact_image_url : {}", id);

        alexa_fact_image_urlRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
