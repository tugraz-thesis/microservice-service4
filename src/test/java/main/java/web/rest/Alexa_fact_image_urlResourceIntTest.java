package main.java.web.rest;

import main.java.Service4App;

import main.java.domain.Alexa_fact_image_url;
import main.java.repository.Alexa_fact_image_urlRepository;
import main.java.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static main.java.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Alexa_fact_image_urlResource REST controller.
 *
 * @see Alexa_fact_image_urlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Service4App.class)
public class Alexa_fact_image_urlResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    @Autowired
    private Alexa_fact_image_urlRepository alexa_fact_image_urlRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAlexa_fact_image_urlMockMvc;

    private Alexa_fact_image_url alexa_fact_image_url;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Alexa_fact_image_urlResource alexa_fact_image_urlResource = new Alexa_fact_image_urlResource(alexa_fact_image_urlRepository);
        this.restAlexa_fact_image_urlMockMvc = MockMvcBuilders.standaloneSetup(alexa_fact_image_urlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Alexa_fact_image_url createEntity(EntityManager em) {
        Alexa_fact_image_url alexa_fact_image_url = new Alexa_fact_image_url()
            .url(DEFAULT_URL);
        return alexa_fact_image_url;
    }

    @Before
    public void initTest() {
        alexa_fact_image_url = createEntity(em);
    }

    @Test
    @Transactional
    public void createAlexa_fact_image_url() throws Exception {
        int databaseSizeBeforeCreate = alexa_fact_image_urlRepository.findAll().size();

        // Create the Alexa_fact_image_url
        restAlexa_fact_image_urlMockMvc.perform(post("/api/alexa-fact-image-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alexa_fact_image_url)))
            .andExpect(status().isCreated());

        // Validate the Alexa_fact_image_url in the database
        List<Alexa_fact_image_url> alexa_fact_image_urlList = alexa_fact_image_urlRepository.findAll();
        assertThat(alexa_fact_image_urlList).hasSize(databaseSizeBeforeCreate + 1);
        Alexa_fact_image_url testAlexa_fact_image_url = alexa_fact_image_urlList.get(alexa_fact_image_urlList.size() - 1);
        assertThat(testAlexa_fact_image_url.getUrl()).isEqualTo(DEFAULT_URL);
    }

    @Test
    @Transactional
    public void createAlexa_fact_image_urlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = alexa_fact_image_urlRepository.findAll().size();

        // Create the Alexa_fact_image_url with an existing ID
        alexa_fact_image_url.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAlexa_fact_image_urlMockMvc.perform(post("/api/alexa-fact-image-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alexa_fact_image_url)))
            .andExpect(status().isBadRequest());

        // Validate the Alexa_fact_image_url in the database
        List<Alexa_fact_image_url> alexa_fact_image_urlList = alexa_fact_image_urlRepository.findAll();
        assertThat(alexa_fact_image_urlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAlexa_fact_image_urls() throws Exception {
        // Initialize the database
        alexa_fact_image_urlRepository.saveAndFlush(alexa_fact_image_url);

        // Get all the alexa_fact_image_urlList
        restAlexa_fact_image_urlMockMvc.perform(get("/api/alexa-fact-image-urls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alexa_fact_image_url.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
    }
    

    @Test
    @Transactional
    public void getAlexa_fact_image_url() throws Exception {
        // Initialize the database
        alexa_fact_image_urlRepository.saveAndFlush(alexa_fact_image_url);

        // Get the alexa_fact_image_url
        restAlexa_fact_image_urlMockMvc.perform(get("/api/alexa-fact-image-urls/{id}", alexa_fact_image_url.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(alexa_fact_image_url.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingAlexa_fact_image_url() throws Exception {
        // Get the alexa_fact_image_url
        restAlexa_fact_image_urlMockMvc.perform(get("/api/alexa-fact-image-urls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAlexa_fact_image_url() throws Exception {
        // Initialize the database
        alexa_fact_image_urlRepository.saveAndFlush(alexa_fact_image_url);

        int databaseSizeBeforeUpdate = alexa_fact_image_urlRepository.findAll().size();

        // Update the alexa_fact_image_url
        Alexa_fact_image_url updatedAlexa_fact_image_url = alexa_fact_image_urlRepository.findById(alexa_fact_image_url.getId()).get();
        // Disconnect from session so that the updates on updatedAlexa_fact_image_url are not directly saved in db
        em.detach(updatedAlexa_fact_image_url);
        updatedAlexa_fact_image_url
            .url(UPDATED_URL);

        restAlexa_fact_image_urlMockMvc.perform(put("/api/alexa-fact-image-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAlexa_fact_image_url)))
            .andExpect(status().isOk());

        // Validate the Alexa_fact_image_url in the database
        List<Alexa_fact_image_url> alexa_fact_image_urlList = alexa_fact_image_urlRepository.findAll();
        assertThat(alexa_fact_image_urlList).hasSize(databaseSizeBeforeUpdate);
        Alexa_fact_image_url testAlexa_fact_image_url = alexa_fact_image_urlList.get(alexa_fact_image_urlList.size() - 1);
        assertThat(testAlexa_fact_image_url.getUrl()).isEqualTo(UPDATED_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingAlexa_fact_image_url() throws Exception {
        int databaseSizeBeforeUpdate = alexa_fact_image_urlRepository.findAll().size();

        // Create the Alexa_fact_image_url

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restAlexa_fact_image_urlMockMvc.perform(put("/api/alexa-fact-image-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alexa_fact_image_url)))
            .andExpect(status().isBadRequest());

        // Validate the Alexa_fact_image_url in the database
        List<Alexa_fact_image_url> alexa_fact_image_urlList = alexa_fact_image_urlRepository.findAll();
        assertThat(alexa_fact_image_urlList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAlexa_fact_image_url() throws Exception {
        // Initialize the database
        alexa_fact_image_urlRepository.saveAndFlush(alexa_fact_image_url);

        int databaseSizeBeforeDelete = alexa_fact_image_urlRepository.findAll().size();

        // Get the alexa_fact_image_url
        restAlexa_fact_image_urlMockMvc.perform(delete("/api/alexa-fact-image-urls/{id}", alexa_fact_image_url.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Alexa_fact_image_url> alexa_fact_image_urlList = alexa_fact_image_urlRepository.findAll();
        assertThat(alexa_fact_image_urlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Alexa_fact_image_url.class);
        Alexa_fact_image_url alexa_fact_image_url1 = new Alexa_fact_image_url();
        alexa_fact_image_url1.setId(1L);
        Alexa_fact_image_url alexa_fact_image_url2 = new Alexa_fact_image_url();
        alexa_fact_image_url2.setId(alexa_fact_image_url1.getId());
        assertThat(alexa_fact_image_url1).isEqualTo(alexa_fact_image_url2);
        alexa_fact_image_url2.setId(2L);
        assertThat(alexa_fact_image_url1).isNotEqualTo(alexa_fact_image_url2);
        alexa_fact_image_url1.setId(null);
        assertThat(alexa_fact_image_url1).isNotEqualTo(alexa_fact_image_url2);
    }
}
